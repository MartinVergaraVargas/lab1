#!/usr/bin/env python3
# -*- coding: utf-8 -*-

class Estanque():
	def __init__(self, combustible):
		self.combustible = combustible

	def gasto(self, combustible, accion, velocidad, distancia, cilindrada):
		
		if accion == 0:
			combustible = (combustible * 0.99)
		else:
			if velocidad == 120:
				
				if cilindrada == 1.2:
					litros = (distancia / 20)
				else:
					litros = (distancia / 14)
					
				if litros >= combustible:
					combustible = 0
					
				else:
					combustible = (combustible - litros)
			
			else:
				
				if cilindrada == 1.2:
					litros = (distancia / 18)
				else:
					litros = (distancia / 12)
					
				if litros >= combustible:
					combustible = 0
					
				else:
					combustible = (combustible - litros)

		return combustible
